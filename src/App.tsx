import { useState } from 'react'
import words from './wordList.json'

function App() {
  const [wordToGuess, setWordToGuess] = useState(() => {
    return words[Math.floor(Math.random()*words.length)]
  })
  const [guessedWord, setGuessedWord] = useState<string[]>([]);
  console.log(wordToGuess)
  return (
    <></>
  )
}

export default App
